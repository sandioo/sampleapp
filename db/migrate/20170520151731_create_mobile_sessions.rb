class CreateMobileSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :mobile_sessions do |t|
      t.integer :user_id
      t.string :session_key
      t.string :string
      t.string :expired_at
      t.string :datetime

      t.timestamps
    end
  end
end
