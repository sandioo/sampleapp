class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :student_name
      t.integer :team_id
      t.string :phone_no

      t.timestamps
    end
  end
end
