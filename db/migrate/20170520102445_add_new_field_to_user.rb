class AddNewFieldToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :user_name, :string
  	add_column :users, :phone_no, :string
  	add_column :users, :password, :string
  end
end
