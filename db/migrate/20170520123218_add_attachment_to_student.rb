class AddAttachmentToStudent < ActiveRecord::Migration[5.0]
  def self.up
    change_table :students do |t|
      t.attachment :photo1
    end
  end

  def self.down
    remove_attachment :students, :photo1
  end
end
