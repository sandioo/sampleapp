Rails.application.routes.draw do
  
  resources :cities
  devise_for :users, skip: :registrations
  resources :students
  resources :teams
  resources :users
  root 'students#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # API
  post "api/v1/authenticate" => "api/version1#authenticate"
  get "api/v1/teams" => "api/version1#get_teams"
  get "api/v1/students" => "api/version1#get_students"
  get "api/v1/cities" => "api/version1#get_cities"
  get "api/v1/team_details/:team_id" => "api/version1#get_team_details"
end
