class StudentSerializer < ActiveModel::Serializer
      attributes :id, :student_name, :email, :team_id, :team_name, :phone_no, :original_image1_url, :medium_image1_url

      def team_name
             TeamSerializer.new(object.team).attributes[:team_name]
      end
    
      def email
             object[:email].nil? || object[:email].blank? ? "" : object[:email]
      end
    
      def original_image1_url
             if object[:photo1_file_name].nil? || object[:photo1_file_name].blank?
                    ""
             else
                    "/sample_app_images/students/photo1s/#{object[:id]}/original/#{object[:photo1_file_name]}"
             end
      end
 
      def medium_image1_url
             if object[:photo1_file_name].nil? || object[:photo1_file_name].blank?
                    ""
            else
                    "/sample_app_images/students/photo1s/#{object[:id]}/medium/#{object[:photo1_file_name]}"
             end
      end
end
