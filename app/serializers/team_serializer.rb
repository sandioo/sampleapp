class TeamSerializer < ActiveModel::Serializer
	attributes :id, :team_name, :created_at

	has_many :students

	def created_at
		object[:created_at].strftime("%Y/%m/%d %H:%M:%S")
	end   
end