class CitySerializer < ActiveModel::Serializer
  attributes :title, :desc, :images
  
  def desc
    if object[:description].nil? || object[:description].blank?
      ""
    else
     object[:description]
   	end
   end
   def images
    if object[:images].nil? || object[:images].blank?
      ""
    else
     object[:images]
    end
	end
end
