json.extract! student, :id, :student_name, :team_id, :phone_no, :created_at, :updated_at
json.url student_url(student, format: :json)
