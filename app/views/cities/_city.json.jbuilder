json.extract! city, :id, :title, :description, :images, :created_at, :updated_at
json.url city_url(city, format: :json)
