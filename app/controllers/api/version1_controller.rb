require 'net/http'
class Api::Version1Controller < ApplicationController
  http_basic_authenticate_with name: "admin", password: "123"
  
  skip_before_filter  :verify_authenticity_token

  before_action :authenticate_session, except: :authenticate

  # POST Authenticate to get Session Kye
  def authenticate
    email = params[:email]
    # phone_no = params[:phone_no]
    pwd = params[:password]
            
    # Check if there are parameters
    render json: {message: "Invalid request."}.to_json, status: 401 and return if email.nil? or pwd.nil? or pwd.blank?

    u = User.find_by_email(email)
    render json: {message: "Invalid email or password."}.to_json, status: 401 and return if u.nil?

    # Check with mobile session
    ms = MobileSession.login(u.email, pwd)
    render json: {message: "Invalid email or password."}.to_json, status: 401 and return if ms.nil?

    # Return session key
    render json: {session_key: ms.session_key, user_name: u.user_name}.to_json, status: 200
  end
	
  # Get teams
  def get_teams
    teams = Array.new
    Team.all.each do |t|
            obj_team = {id: t.id, team_name: t.team_name, member_count: t.students.count, created_at: t.created_at.strftime("%Y/%m/%d %H:%M:%S") }
            teams << obj_team
    end
    render json: teams.to_json, status: 200
  end

 # Get team details
  def get_team_details
    team = TeamSerializer.new(Team.find(params[:team_id]))
    render json: team.to_json, status: 200
  end

  # Get student_list
  def get_students
    students = Array.new
    Student.all.each do |t|
      students << StudentSerializer.new(Student.find(t))
    end
    render json: students.to_json, status: 200
  end

  # Get city list
  def get_cities
    cities = Array.new
    City.all.each do |t|
      cities << CitySerializer.new(City.find(t))
    end
    render json: cities.to_json, status: 200
  end
    
  protected
  def authenticate_session
    # Check session key
    skey = params[:session_key]
    render json: {message: "Required authentication."}.to_json, status: 401 and return false if skey.nil?

    # Get Session
    @mobile_user = MobileSession.get_user(skey)
    render json: {message: "Invalid/expired session."}.to_json, status: 401 and return false if @mobile_user.nil?

    return true
  end
  
end