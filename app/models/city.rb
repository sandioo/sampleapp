class City < ApplicationRecord
	validates :title, presence: true, uniqueness: true
end
