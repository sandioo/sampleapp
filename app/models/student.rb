class Student < ApplicationRecord
	after_save :create_user

	validates :student_name, presence: true, uniqueness: true
	validates :phone_no, presence: true

	belongs_to :team

	has_attached_file :photo1,
	:styles => {:medium => "200x200>", :thumb => "80x80>" },
	:default_url => "logo.png",
	:path => ":rails_root/public/sample_app_images/students/:attachment/:id/:style/:filename",
	:url => "/sample_app_images/students/:attachment/:id/:style/:filename"

	validates_attachment :photo1,
	# :presence => true,
	:content_type => { :content_type => ["image/jpg","image/jpeg", "image/gif", "image/png"] },
	:size => { :less_than => 2.4.megabytes, :message => "is over 2.4MB. Please choose another file."}


	def create_user
  		 User.create(user_name: self.student_name, email: self.email, phone_no: self.phone_no, password: "123456") unless User.exists?(email: self.email)
  	end
end
