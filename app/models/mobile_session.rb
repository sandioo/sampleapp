require 'securerandom'
class MobileSession < ApplicationRecord

    belongs_to :user

    # Get respective user of given Mobile session key
    def self.get_user(session_key)
        # Find session with given key
        ms = MobileSession.find_by_session_key(session_key)
        return nil if ms.nil?
        
        # If found and already expired. Delete and retur nil
        if ms.is_expired?
            ms.delete
            return nil
        end

        # Otherwise
        return ms.user 
    end

    # Authenticate & create mobile session if successful
    def self.login(email, password)
        u = User.find_by_email(email)

        if u.valid_password?(password)
            u.mobile_session.delete unless u.mobile_session.nil? # Delete all existing Sessions
            ms = MobileSession.create(
                user_id: u.id, 
                session_key: SecureRandom.hex(20), 
                expired_at: DateTime.now + 5.days)
            raise "Fatal error: Can't create mobile session." if ms.nil?
            return ms
        end unless u.nil?

        return nil
    end

    def is_expired?
        return false
        #NOTE return DateTime.now >= expired_at
    end
end
