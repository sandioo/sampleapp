class Team < ApplicationRecord
	validates :team_name, presence: true, uniqueness: true

	has_many :students
end
